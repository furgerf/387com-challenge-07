//
//  EditContactViewController.swift
//  CoreContacts
//
//  Created by admin on 11/29/14.
//  Copyright (c) 2014 Fabian Furger. All rights reserved.
//

import UIKit

class EditContactViewController: UIViewController {
    
    @IBOutlet weak var phoneHeight: NSLayoutConstraint!
    @IBOutlet weak var phoneNumbers: UITextView!
    
    @IBAction func addPhone(sender: UIButton) {
        var type:String?
        var number:UITextField?
        var alert = UIAlertController(title: "New phone", message: "Add new phone type and number", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addTextFieldWithConfigurationHandler({
            (textField) -> Void in
            number = textField
            textField.placeholder = "Phone number"
        })
        
        var typeSelected:(UIAlertAction) -> Void = {
            (action) -> Void in

            if number!.text.isEmpty {
                var otherAlert = UIAlertController(title: "Add number", message: "You forgot to type in the number!", preferredStyle: UIAlertControllerStyle.Alert)
                otherAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {(action) -> Void in
                    self.presentViewController(alert, animated: true, completion: nil)
                }))
                
                self.presentViewController(otherAlert, animated: true, completion: nil)
            } else {
                println("Adding " + action.title + " phone with number " + number!.text)
                self.phoneNumberArray.append(number!.text)
                self.phoneTypeArray.append(PhoneType(rawValue: action.title)!)
                
                self.phoneHeight.constant = self.phoneHeight.constant == 0 ? 32 : self.phoneHeight.constant + 14
                
                self.phoneNumbers.text = (self.phoneNumbers.text.isEmpty ? "" : (self.phoneNumbers.text + "\n")) + "\(action.title):\t\(number!.text)"
            }
        }
        alert.addAction(UIAlertAction(title: "Mobile", style: UIAlertActionStyle.Default, handler: {(action) -> Void in typeSelected(action)}))
        alert.addAction(UIAlertAction(title: "Home", style: UIAlertActionStyle.Default, handler: {(action) -> Void in typeSelected(action)}))
        alert.addAction(UIAlertAction(title: "Work", style: UIAlertActionStyle.Default, handler: {(action) -> Void in typeSelected(action)}))
        alert.addAction(UIAlertAction(title: "Other", style: UIAlertActionStyle.Default, handler: {(action) -> Void in typeSelected(action)}))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
                
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func removePhone(sender: UIButton) {
        if self.phoneTypeArray.isEmpty {
            return
        }
        
        var alert = UIAlertController(title: "Confirm remove", message: "Do you want to remove\n\"\(phoneTypeArray.last!.rawValue):\t\(phoneNumberArray.last!)\"", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {(action) -> Void in
            self.phoneTypeArray.removeLast()
            self.phoneNumberArray.removeLast()

            self.phoneHeight.constant = self.phoneHeight.constant == 32 ? 0 : self.phoneHeight.constant - 14
            
            self.phoneNumbers.text = ""
            
            for (i, e) in enumerate(self.phoneNumberArray) {
                if self.phoneNumbers.text != "" {
                    self.phoneNumbers.text = self.phoneNumbers.text + "\n"
                }
                
                self.phoneNumbers.text = self.phoneNumbers.text + "\(self.phoneTypeArray[i].rawValue):\t\(self.phoneNumberArray[i])"
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func setBirthday(sender: UIButton) {
        var day:UITextField?
        var month:UITextField?
        var year:UITextField?
        
        let calendar = NSCalendar.currentCalendar()
        var components:NSDateComponents? = nil
        if let dob = self.birthday {
            components = calendar.components(.DayCalendarUnit | .MonthCalendarUnit | .YearCalendarUnit, fromDate: self.birthday!)
        }
        
        var alert = UIAlertController(title: "Set birthday", message: "Set the contact's birthday", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addTextFieldWithConfigurationHandler({(textField) -> Void in
            if let c = components {
                textField.text = c.day.description
            }
            textField.placeholder = "Day"
            day = textField
        })
        alert.addTextFieldWithConfigurationHandler({(textField) -> Void in
            if let c = components {
                textField.text = c.month.description
            }
            textField.placeholder = "Month"
            month = textField
        })
        alert.addTextFieldWithConfigurationHandler({(textField) -> Void in
            if let c = components {
                textField.text = c.year.description
            }
            textField.placeholder = "Year"
            year = textField
        })
        alert.addAction(UIAlertAction(title: "Set birthday", style: UIAlertActionStyle.Default, handler: {(action) -> Void in
            var d = day!.text.toInt()
            var m = month!.text.toInt()
            var y = year!.text.toInt()
            var valid = true
            var date:NSDate?
            
            let df = NSDateFormatter()
            df.dateFormat = "dd-MM-yyyy"
            
            if d != nil && m != nil && y != nil {
                date = df.dateFromString("\(d!)-\(m!)-\(y!)")
            }
            
            if date == nil {
                var otherAlert = UIAlertController(title: "Invalid date", message: "Please enter a valid date", preferredStyle: UIAlertControllerStyle.Alert)
                otherAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {(action) -> Void in
                    self.presentViewController(alert, animated: true, completion: nil)
                }))
                
                self.presentViewController(otherAlert, animated: true, completion: nil)
                return
            }
            
            self.birthdayLabel.text = "\(d!)/\(m!)/\(y!)"
            println("Set birthday to \(d!)-\(m!)-\(y!)")
            
            self.birthday = date!
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    @IBAction func setAddress(sender: UIButton) {
        var street:UITextField?
        var city:UITextField?
        var postCode:UITextField?
        
        var alert = UIAlertController(title: "Set address", message: "Set the contact's address", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addTextFieldWithConfigurationHandler({(textField) -> Void in
            if let foo = self.addressStreet {
                textField.text = foo
            }
            textField.placeholder = "Street"
            street = textField
        })
        alert.addTextFieldWithConfigurationHandler({(textField) -> Void in
            if let foo = self.addressCity {
                textField.text = foo
            }
            textField.placeholder = "City"
            city = textField
        })
        alert.addTextFieldWithConfigurationHandler({(textField) -> Void in
            if let foo = self.addressPostCode {
                textField.text = foo
            }
            textField.placeholder = "Post code"
            postCode = textField
        })
        alert.addAction(UIAlertAction(title: "Set address", style: UIAlertActionStyle.Default, handler: {(action) -> Void in
            if street!.text.isEmpty && city!.text.isEmpty && postCode!.text.isEmpty {
                var otherAlert = UIAlertController(title: "No address", message: "Please enter an address", preferredStyle: UIAlertControllerStyle.Alert)
                otherAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {(action) -> Void in
                    self.presentViewController(alert, animated: true, completion: nil)
                }))
                
                self.presentViewController(otherAlert, animated: true, completion: nil)
                return
            }
            self.addressStreet = street!.text
            self.addressCity = city!.text
            self.addressPostCode = postCode!.text
            
            self.addressLabel.text = (self.addressStreet != nil && self.addressCity != nil) ? (self.addressStreet! + ", " + self.addressCity!) : ((self.addressStreet ?? "") + (self.addressCity ?? ""))
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func deleteContact(sender: UIButton) {
        var alert = UIAlertController(title: "Confirm delete", message: "Are you sure you want to delete the contact?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {(action) -> Void in
            contacts.removeContact(self.originalContact!)
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func saveContact(sender: UIButton) {
        if (self.firstName.text == nil || self.firstName.text.isEmpty) && (self.lastName.text == nil || self.lastName.text.isEmpty) {
            var alert = UIAlertController(title: "No name set", message: "Cant save contact because no name has been set", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        var groupString = self.group.titleForSegmentAtIndex(self.group.selectedSegmentIndex)
        
        if let contact = originalContact {
            contacts.editContact(contact, firstName: self.firstName.text, lastName: self.lastName.text, dob: self.birthday, group: groupString == "Other" ? nil : Group(rawValue: groupString!), addressStreet: self.addressStreet, addressCity: self.addressCity, addressPostCode: self.addressPostCode, phoneNumbers: self.phoneNumberArray, phoneTypes: self.phoneTypeArray)
        } else {
            contacts.addContact(firstName: self.firstName.text, lastName: self.lastName.text, dob: self.birthday, group: groupString == "Other" ? nil : Group(rawValue: groupString!), addressStreet: self.addressStreet, addressCity: self.addressCity, addressPostCode: self.addressPostCode, phoneNumbers: self.phoneNumberArray, phoneTypes: self.phoneTypeArray)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancelContact(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet weak var birthdayLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var group: UISegmentedControl!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    
    @IBOutlet var swipeDown: UISwipeGestureRecognizer!
    @IBAction func swipe(sender: UISwipeGestureRecognizer) {
        println("Swipe down")
        self.firstName.resignFirstResponder()
        self.lastName.resignFirstResponder()
    }
    var birthday:NSDate?
    
    var phoneTypeArray:[PhoneType] = []
    
    var phoneNumberArray:[String] = []
    
    var addressStreet:String?
    var addressCity:String?
    var addressPostCode:String?
    
    var originalContact:Contact?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.phoneHeight.constant = 0
        
        self.view.addGestureRecognizer(self.swipeDown)

        if let contact = originalContact {
            self.firstName.text = contact.firstName
            self.lastName.text = contact.lastName
            if let grp = contact.group {
                for var i = 0; i < self.group.numberOfSegments; i++ {
                    if self.group.titleForSegmentAtIndex(i) == grp {
                        self.group.selectedSegmentIndex = i
                        break
                    }
                }
            }
            if let dob = contact.dob {
                self.birthday = dob
                let calendar = NSCalendar.currentCalendar()
                let components = calendar.components(.YearCalendarUnit | .MonthCalendarUnit | .DayCalendarUnit, fromDate: dob)

                self.birthdayLabel.text = "\(components.day)/\(components.month)/\(components.year)"
            }
            if let address = contact.livesAt {
                self.addressStreet = address.street
                self.addressCity = address.city
                self.addressPostCode = address.postCode
                
                self.addressLabel.text = (self.addressStreet != nil && self.addressCity != nil) ? (self.addressStreet! + ", " + self.addressCity!) : ((self.addressStreet ?? "") + (self.addressCity ?? ""))
            }
            if let set = contact.hasPhone {
                let phones = set.allObjects as [Phone]
                
                if phones.count > 0 {
                    self.phoneHeight.constant = 32-14
                
                    for (i, p) in enumerate(phones) {
                        self.phoneNumberArray.append(p.number!)
                        self.phoneTypeArray.append(PhoneType(rawValue: p.type!)!)
                    
                        self.phoneHeight.constant += 14
                        self.phoneNumbers.text = (self.phoneNumbers.text.isEmpty ? "" : (self.phoneNumbers.text + "\n")) + "\(p.type!):\t\(p.number!)"
                    }
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }
}
