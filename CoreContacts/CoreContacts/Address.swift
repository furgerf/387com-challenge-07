//
//  Address.swift
//  CoreContacts
//
//  Created by admin on 11/29/14.
//  Copyright (c) 2014 Fabian Furger. All rights reserved.
//

import Foundation
import CoreData

class Address: NSManagedObject {

    @NSManaged var city: String?
    @NSManaged var postCode: String?
    @NSManaged var street: String?
    @NSManaged var inhabitant: NSSet

}
