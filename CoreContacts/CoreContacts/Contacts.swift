//
//  Contacts.swift
//  CoreContacts
//
//  Created by admin on 11/28/14.
//  Copyright (c) 2014 Fabian Furger. All rights reserved.
//

import UIKit
import CoreData

let contacts = Contacts()

enum Group: String {
    case Family = "Family"
    case Friends = "Friends"
    case Work = "Work"
}

enum PhoneType: String {
    case Mobile = "Mobile"
    case Home = "Home"
    case Work = "Work"
    case Other = "Other"
}

extension String {
    subscript (i: Int) -> String {
        return String(Array(self)[i])
    }
}

extension Array {
    func contains<T : Equatable>(obj: T) -> Bool {
        let filtered = self.filter {$0 as? T == obj}
        return filtered.count > 0
    }
}

class Contacts {
    var visibleGroup:Group? = nil
    
    var moc:NSManagedObjectContext? = nil
    
    var contacts:[String:[Contact]] = [:]
    
    var onContactsChanged:(() -> Void)? = nil
    
    func getIthLetter(i:Int) -> String {
        return String(UnicodeScalar(i + 65))
    }
    
    func setVisibleGroup(group: Group?) {
        if group == self.visibleGroup {
            return
        }
        
        self.visibleGroup = group
        self.loadData()
        
        if self.onContactsChanged != nil {
            self.onContactsChanged!()
        }
    }

    func initialize(context: NSManagedObjectContext){
        self.moc = context
        
        if self.loadData() {
            self.addData()
        }
    }
    
    func loadData(nameFilter: String = "*") -> Bool {
        for (var i = 0; i < 26; i++) {
            self.contacts[getIthLetter(i)] = []
        }
        
        var isEmpty = true
        for (index, contact) in enumerate(getSavedContacts(nameString: nameFilter.isEmpty ? "*" : "*" + nameFilter + "*")) {
            self.addContactToArray(contact)
            isEmpty = false
        }
        
        return isEmpty
    }
    
    func addContactToArray(contact: Contact) {
        var name = contact.getDisplayName()
        var char = name.uppercaseString[0]
        var i = 0
        
        for (;i < self.contacts[char]?.count;i++) {
            var c = self.contacts[char]![i]
            var n = c.getDisplayName()
            
            if n > name {
                break
            }
        }
        
        self.contacts[char]?.insert(contact, atIndex: i)
    }
    
    func removeContactFromArray(contact: Contact) -> Bool {
        var name = contact.getDisplayName()
        var char = name.uppercaseString[0]
        
        for (var i = 0; i < self.contacts[char]?.count;i++) {
            if self.contacts[char]![i] == contact {
                self.contacts[char]?.removeAtIndex(i)
                println("removed contact from array")
                return true
            }
        }
        return false
    }
    
    func getContactsByLetter(letter:String) -> [Contact] {
        return self.contacts.keys.array.contains(letter) ? self.contacts[letter.uppercaseString[0]]! : []
    }
    
    func getSavedContacts(nameString:String = "*") -> [Contact] {
        var groupString = self.visibleGroup != nil ? self.visibleGroup!.rawValue : "*"
        
        let groupPredicate = NSPredicate(format: "SELF.group LIKE %@",  groupString)
        let namePredicate = NSPredicate(format: "SELF.firstName LIKE[cd] %@ OR SELF.lastName LIKE[cd] %@", nameString, nameString)
        
        let compoundPredicate = NSCompoundPredicate.andPredicateWithSubpredicates([groupPredicate!, namePredicate!])
        
        let fetchRequest = NSFetchRequest(entityName: "Contact")
        fetchRequest.predicate = compoundPredicate
        
        if let fetchResults = self.moc!.executeFetchRequest(fetchRequest, error: nil) as? [Contact] {
            println("Found " + fetchResults.count.description + " contacts")
            return fetchResults
        }

        println("ERROR retrieving contacts")
        
        return []
    }
    
    func removeContact(contact:Contact, ignoreEvent:Bool = false) {
        if !self.removeContactFromArray(contact) {
            println("Couldn't remove contact from array, attempting to recover by re-loading data")
            self.loadData()
        }
        
        self.moc?.deleteObject(contact)
        self.moc?.save(nil)
        
        println("Deleted contact")
        
        if let contactsChanged = self.onContactsChanged {
            if !ignoreEvent {
                contactsChanged()
            }
        }
    }
    
    func editContact(oldContact:Contact, firstName:String? = nil, lastName:String? = nil, dob:NSDate? = nil, group:Group? = nil, addressStreet:String? = nil, addressCity:String? = nil, addressPostCode:String? = nil, phoneNumbers:[String]? = nil, phoneTypes:[PhoneType]? = nil) {
        println("Contact editing not implemented")
        
        self.addContact(firstName: firstName, lastName: lastName, dob: dob, group: group, addressStreet: addressStreet, addressCity: addressCity, addressPostCode: addressPostCode, phoneNumbers: phoneNumbers, phoneTypes: phoneTypes)
        
        self.removeContact(oldContact, ignoreEvent: true)
        
        println("saving context after editing")
        self.moc?.save(nil)
        
        println("contact editing complete")
        if let contactsChanged = self.onContactsChanged {
            contactsChanged()
        }
    }
    
    func addContact(firstName:String? = nil, lastName:String? = nil, dob:NSDate? = nil, group:Group? = nil, addressStreet:String? = nil, addressCity:String? = nil, addressPostCode:String? = nil, phoneNumbers:[String]? = nil, phoneTypes:[PhoneType]? = nil) {
        
        let nc:Contact = NSEntityDescription.insertNewObjectForEntityForName("Contact", inManagedObjectContext: self.moc!) as Contact
        
        // contact fields
        if let fn = firstName {
            nc.firstName = fn
        }
        if let ln = lastName {
            nc.lastName = ln
        }
        if firstName == nil && lastName == nil {
            nc.lastName = "unknown"
        }
        if let db = dob {
            nc.dob = db
        }
        if let gr = group {
            nc.group = gr.rawValue
        }
        
        // address
        if addressStreet != nil || addressCity != nil || addressPostCode != nil {
            let ad:Address = NSEntityDescription.insertNewObjectForEntityForName("Address", inManagedObjectContext: self.moc!) as Address
        
            if let street = addressStreet {
                ad.street = street
            }
            if let city = addressCity {
                ad.city = city
            }
            if let postCode = addressPostCode {
                ad.postCode = postCode
            }
            nc.livesAt = ad
        }
        
        if phoneNumbers != nil && phoneTypes != nil {
            var min = phoneNumbers!.count < phoneTypes!.count ? phoneNumbers!.count : phoneTypes!.count
            var phones:[Phone] = []
            
            for (i, unused) in enumerate(phoneNumbers!) {
                let p:Phone = NSEntityDescription.insertNewObjectForEntityForName("Phone", inManagedObjectContext: self.moc!) as Phone
                p.number = phoneNumbers![i]
                p.type = phoneTypes![i].rawValue
                phones.append(p)
            }
            
            nc.hasPhone = NSSet(array: phones)
        }
        
        self.moc?.save(nil)
        self.addContactToArray(nc)
        println("Added new contact " + nc.getDisplayName())
        
        if let contactsChanged = self.onContactsChanged {
            contactsChanged()
        }
    }
    
    func addData() {
        println("Creating dummy data")
        
        let df = NSDateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        
        // note that field "group" is not assigned
        addContact(firstName: "Fabian", lastName: "Furger", dob: df.dateFromString("24/11/1990"), addressStreet: "Earlsdon Avenue South 64", addressCity: "Coventry", addressPostCode: "CV5 6DT", phoneNumbers: ["1234", "5678"], phoneTypes: [PhoneType.Home, PhoneType.Mobile])
        
        addContact(firstName: "Vera", lastName: "Furger", dob: df.dateFromString("16/6/1996"), group: Group.Family, addressStreet: "Wangweg 2", addressCity: "Stans", addressPostCode: "6370")
        
        addContact(firstName: "Gotti", group: Group.Family, addressStreet: "Dorfplatz 3", addressCity: "Stans", addressPostCode: "6370", phoneNumbers: ["95789374"], phoneTypes: [PhoneType.Home])
    }
}
