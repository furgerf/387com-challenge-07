//
//  Contact.swift
//  CoreContacts
//
//  Created by admin on 11/29/14.
//  Copyright (c) 2014 Fabian Furger. All rights reserved.
//

import Foundation
import CoreData

class Contact: NSManagedObject {

    @NSManaged var dob: NSDate?
    @NSManaged var firstName: String?
    @NSManaged var group: String?
    @NSManaged var lastName: String?
    @NSManaged var livesAt: Address?
    @NSManaged var hasPhone: NSSet?
    
    func getDisplayName() -> String {
        return ((self.firstName != nil && !self.firstName!.isEmpty) && (self.lastName != nil && !self.lastName!.isEmpty)) ?
            self.lastName! + ", " + self.firstName! :
            (self.firstName ?? "") + (self.lastName ?? "")
    }

}
