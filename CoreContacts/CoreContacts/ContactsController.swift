//
//  ContactsController.swift
//  CoreContacts
//
//  Created by admin on 11/29/14.
//  Copyright (c) 2014 Fabian Furger. All rights reserved.
//

import UIKit

class ContactsController: UITableViewController, UISearchBarDelegate {

    @IBOutlet var contactsTable: UITableView!
    
    @IBAction func searchButtonClick(sender: UIBarButtonItem) {
        if self.searchBar.frame.height == 0 {
            self.searchBar.frame.size.height = 44
            self.contactsTable.frame.offset(dx: 0, dy: 44)
            self.contactsTable.frame.size.height -= 44
        } else {
            self.searchBar.frame.size.height = 0
            self.contactsTable.frame.offset(dx: 0, dy: -44)
            self.contactsTable.frame.size.height += 44

        }
    }
    @IBAction func selectContacts(sender: UIButton) {
        var alert = UIAlertController(title: "Select group", message: "Which group do you want to see?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Family", style: UIAlertActionStyle.Default, handler: {(action) -> Void in
            contacts.setVisibleGroup(.Family)
            self.contactSelector.setTitle("Family", forState: UIControlState.Normal)
        }))
        alert.addAction(UIAlertAction(title: "Friends", style: UIAlertActionStyle.Default, handler: {(action) -> Void in
            contacts.setVisibleGroup(.Friends)
            self.contactSelector.setTitle("Friends", forState: UIControlState.Normal)
        }))
        alert.addAction(UIAlertAction(title: "Work", style: UIAlertActionStyle.Default, handler: {(action) -> Void in
            contacts.setVisibleGroup(.Work)
            self.contactSelector.setTitle("Colleagues", forState: UIControlState.Normal)
        }))
        alert.addAction(UIAlertAction(title: "All groups", style: UIAlertActionStyle.Default, handler: {(action) -> Void in
            contacts.setVisibleGroup(nil)
            self.contactSelector.setTitle("All Contacts", forState: UIControlState.Normal)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBOutlet weak var contactSelector: UIButton!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBar.delegate = self
        
        //self.searchBar.frame.size.height = 0
        
        contacts.initialize((UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext!)
        self.contactsTable.reloadData()
        contacts.onContactsChanged = {
            println("reloading table")
            self.contactsTable.reloadData()
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        contacts.loadData(nameFilter: searchText)
        
        if let contactsChanged = contacts.onContactsChanged {
            contactsChanged()
        }
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 26
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return contacts.getContactsByLetter(contacts.getIthLetter(section)).count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return contacts.getContactsByLetter(contacts.getIthLetter(section)).count > 0 ? contacts.getIthLetter(section) : ""
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Contact", forIndexPath: indexPath) as UITableViewCell

        var c = contacts.getContactsByLetter(contacts.getIthLetter(indexPath.section))[indexPath.row]
        
        cell.textLabel.text = c.getDisplayName()
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "editContact" {
            println("Preparing for editContact segue")
            
            let destination = segue.destinationViewController as EditContactViewController
            let indexPath = self.tableView.indexPathForSelectedRow()
            destination.originalContact = contacts.getContactsByLetter(contacts.getIthLetter(indexPath!.section))[indexPath!.row]
        }
        if segue.identifier == "addContact" {
            println("Preparing for addContact segue")
        }
    }

}
