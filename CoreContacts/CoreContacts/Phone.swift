//
//  Phone.swift
//  CoreContacts
//
//  Created by admin on 11/29/14.
//  Copyright (c) 2014 Fabian Furger. All rights reserved.
//

import Foundation
import CoreData

class Phone: NSManagedObject {

    @NSManaged var number: String?
    @NSManaged var type: String?
    @NSManaged var owner: Contact

}
